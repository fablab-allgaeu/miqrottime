FROM debian:stretch-slim

ENV DEBIAN_FRONTEND noninteractive

ARG APT_FLAGS_COMMON="--no-install-recommends -qy"
RUN apt-get ${APT_FLAGS_COMMON} update && \
    apt-get ${APT_FLAGS_COMMON} install \
        autoconf \
        automake \
        bash \
        bison \
        bzip2 \
        ca-certificates \
        flex \
        g++ \
        gawk \
        gcc \
        git \
        gperf \
        help2man \
        libexpat-dev \
        libtool \
        libtool-bin \
        make \
        ncurses-dev \
        patch \
        python \
        python-dev \
        python-serial \
        sed \
        texinfo \
        unrar-free \
        wget && \
    apt-get ${APT_FLAGS_COMMON} autoremove && \
    apt-get ${APT_FLAGS_COMMON} clean && \
    rm -rf /var/lib/apt/lists/*

RUN useradd -m -U -s /bin/bash esp
USER esp
WORKDIR /home/esp

# Build the toolchain and C library
RUN git clone --recurse-submodules https://github.com/pfalcon/esp-open-sdk.git && \
    cd esp-open-sdk && \
    make STANDALONE=y

# Build SDK
RUN git clone https://github.com/espressif/ESP8266_RTOS_SDK.git
